function init_page(bhv,id){
  	if(id!==null){
      var kurl=baseUrl + "/get?gt=tm&ac=get&tp=article&id="+id+"&tag=text&hv="+bhv+"&mo=book";
    }
	else{
      var kurl=baseUrl + "/get?gt=tm&ac=getBookTM&tp=article&tag=text&hv="+bhv+"&mo=book";
    }
  
$.getJSON(kurl,function(d){
  	var ks=[]
  	if(id!==null){
      ks=pusk_articleKs(d,id);
    }
	else{
      ks=push_bookKs(d);
    }	
  
	const knowledge={
    	data(){
        	return {
              showStyle:'card',
              swiper: {},
              currentCard:'19-3',
              currentKnow: {'know_code':'','rating':-1},
              knowRating: 5,
              screenHeight:'500px',
            	knowledge:ks,
              	pid:0,
              	pidClass:'isprevious',
            }
        },
       mounted(){
         const _this = this
         const ch =document.getElementById('knowledgePage').offsetWidth
         this.screenHeight = parseInt((ch/screen.width)*screen.height) + 'px'         
         console.log('20230329-----',screen.width,screen.height,ch, this.screenHeight)
         this.swiper = new Swiper('.swiper', {
           // Optional parameters
           height: parseInt((ch/screen.width)*screen.height),
           //direction: 'vertical',
           allowTouchMove: true,
           loop: false,
         });
         console.log(this.swiper.activeIndex)
         _this.currentKnow['know_code'] = bhv+'-'+_this.swiper.activeIndex
         _this.init_know()
         this.swiper.on('realIndexChange', function () {
           _this.currentKnow['know_code'] = bhv+'-'+_this.swiper.activeIndex
           _this.init_know()
           console.log(_this.swiper.activeIndex);
         })
       },
        //watch: {
        //  // whenever question changes, this function will run
        //  swiper(newSwiper, oldSwiper) {
        //    if (newSwiper.activeIndex != oldSwiper.activeIndex){
        //       this.currentKnow['know_code'] = bhv+'-'+this.swiper.activeIndex
        //       this.init_know()
        //    }
        //  }
        //},
      	methods:{
          changeKnowRating(){
            if(this.knowRating != this.currentKnow['rating']){
              this.currentKnow['rating'] = this.knowRating
              this.update_know_rating()
            }
            console.log('hii')
          },
          showMore(t){
             const e = document.getElementById(t)
             e.style.overflow = e.style.overflow != 'scroll' ? 'scroll' : 'hidden'
             console.log(t)
           },
           async init_know(){
              const options = {
                url: baseApiUrl + '/api/v1/know/?know_code=' + this.currentKnow['know_code'],
                method: 'GET',
              }
              const s = await axios.request(options)
              this.currentKnow['know_code'] = s.data['know_code']
              this.knowRating = s.data['rating']
              this.currentKnow['rating'] = s.data['rating']
           },          
           async update_know_rating(){
              const options = {
                url: baseApiUrl + '/api/v1/know/rating/',
                method: 'PUT',  
                data: {
                   'know_code': this.currentKnow['know_code'],
                   'rating': this.currentKnow['rating'],
                   'updateRating': this.knowRating,
                }
              }
              const s = await axios.request(options)
           },
        	view_article(id){
              	window.open(baseUrl + "/view?vt=page&hv=123554b514bb4&tp=article&tag=text&uhv=1236c7672f3cb&mo=book&thv="+bhv+"&id="+id)
            	console.log("view")
            },
          	isPrevious(id){
              return this.pidClass
            	if(this.pid===id){
                  	return this.pidClass
                }
              	else{
                	this.pid=id
                  	this.pidClass=this.pidClass.split('').reverse().join('')
                  	return this.pidClass
                }
            }
        }
    }
    
    Vue.createApp(knowledge).use(Quasar).mount('#knowledgePage')
});

}

function pusk_articleKs(d,id){
  ks=[]
  for(let k in d.knowledge){
    try{
      if(d.knowledge[k]['text'].length>0){
        d.knowledge[k]['id']=id;
        ks.push(d.knowledge[k])
      }
    }
    catch(err){console.log(err)}
  }
  return ks
}

function push_bookKs(d){
  	ks=[]
	for(let ti in d){
      	  let dtij=JSON.parse(d[ti])
          for(let k in dtij.knowledge){
              try{
                if(dtij.knowledge[k]['text'].length>0){
                  	  dtij.knowledge[k]['id']=ti;
                      ks.push(dtij.knowledge[k])
                   }
                }
              catch(err){console.log(err)}
          }
          //console.log(ks);
    }
  	return ks
}


function init_knowledgePage(){

var kurl=baseUrl + "/get?gt=tm&ac=get&id=3&tp=article&tag=text&hv=boo58bf36c30d";
$.getJSON(kurl,function(d){
  	let e=document.createElement("div");
	for (let k in d){
		try{
          console.log(d[k]);
          let kj=k.split(',');
          let dkj=JSON.parse(d[k][0]);
          let el=document.createElement("div");
          el.innerHTML=kj[2];
          el.setAttribute("onclick","view_sources('"+dkj[1]+"');")
          e.appendChild(el);
        }
      	catch(e){console.log(e)}
    }
  	document.body.appendChild(e);

});

}

function view_sources(hv){
	var hvl=hv.split(',');
  	console.log(hvl);
  	window.open("http://192.168.43.100:8080/view?vt=page&hv=123554b514bb4&tp=article&tag=text&mo=book&id="+hvl[3]+"&thv="+hvl[0]);
	
}