const { defineConfig } = require("cypress");

module.exports = defineConfig({
  viewportWidth: 1118,
  viewportHeight: 648,
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
